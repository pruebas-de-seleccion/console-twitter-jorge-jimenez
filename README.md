ConsoleTwitter 
========================

# Welcome!
Hello people!!! 

This is my solution of your challenge. It is written in Kotlin because for me it support very fast develop and a lot of helper to good practices. I hope that you found it well. 

# Description
Ok. I think that I can jump this part because you already know [yours requirement](/INSTUCTIONS.md) :)

## What was my decisions?

- **Use Kotlin**: At first I choose java, but then I thought that I could demonstrate my experience in java given my CV. For that reason I change to kotlin, that help me a lot to develop quickly (this is important because today is saturday and I want to enjoy my free time). **I didn't make any annotation about kotlin language. If you need some help in order to understand better my code about sintaxis or properties of the language please let me know.** 
- **Use DDD**: Ok, for this challenge DDD may be to much. DDD architecture add a lot of  boilerplate to split business logic and in small project like this could be annoying. But I think that the final result is good and that DDD make the code more reusable and maintainable.
- **Use TDD**: You can see it in the commit history. I broke some commit rules because I was in hurry (weekend! xD) but you can prove that almost all code is pushing for some test.
- **Use feature sample to acceptance test**: I love this!!!! Usually I use Object Mother pattern to generate aleatory data to my tests but in this case you give me some good examples and I found funny use exactly those data examples as acceptance criteria. It's true that some tests have some noise because of it, but it is not worried
- **Not overreach**: I decide don't overreach. I don't create anything extra as documentation, extra validation or whatever. If you need any more, only tell me. 
- **Number of command can change IN FUTURE**: I decide that in future commands can change and for this reason I create command concept. However for las point, not overreach, I decide don't invest time to create 'open to extension' command factory. That could be a good next step. 
- **Post printer**: Currently the method to print the time gap for a post is a temporal implementation :(. 

## How is it tested?
At the moment this project has the three more common type of test: unitary, integration and acceptance. All that test are currently in the same folder (test). I know that that decision could be long time discussed and probably it is not the best, but that is a challenge and I thought that it is enough given the context.
- **Acceptance test**:
	- FollowingFeatureShould & ReadingFeatureShould: I model given samples. To check the result I 'read' the lines wrotes in the console. Unfortunately, performing 100% black box testing by typing in the console was more expensive for me, as I had to control the life cycle of the application and provide an output command.   
- **Integration test**: There are not integration tests itself because almost infrastructure stuff have memory implementation. We could create a console integration test but I did not see necessary.
- **Unitary test**: the rest test.

## What could be improved?
- Everything by default :D
- Command factory need love: Maybe we can create a extensible factory based in some pattern like chain of responsibility.
- Test need love: 
	- As result of low time inverted we decide don't solve some interesting things like trim user names and commands,  that could  improve the resilience. 
    - Could be great create a random sampler to use with test purposes. That could show some unknown corner cases.
- Split tests in folder and create split task to run it in gradle
- User klint and checkstyle to check static code rules and formatting.  

## Mandatory Gif
Because a mandatory gif always is funny and improve PR readability. 

![mandatory gif](https://media.giphy.com/media/Mliueouehmpag/giphy.gif)
