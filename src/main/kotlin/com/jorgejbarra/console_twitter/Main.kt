package com.jorgejbarra.console_twitter

import com.jorgejbarra.console_twitter.infrastructure.TwitterFactory

fun main(args: Array<String>) {
    val twitterInstance = TwitterFactory.getTwitterInstance()

    println(
        """That is a simple example that how to use the twitter console library. 
            |
            |Please to exist pres 'ctrl+c'. Happy test!!!! :)
            |> """.trimMargin()
    )

    while (true) {
        readLine()?.apply { twitterInstance.execute(this) }
        print("> ")
    }
}
