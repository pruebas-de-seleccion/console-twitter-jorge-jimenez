package com.jorgejbarra.console_twitter.application

import com.jorgejbarra.console_twitter.application.command.Command.*
import com.jorgejbarra.console_twitter.application.command.CommandFactory
import com.jorgejbarra.console_twitter.domain.ShowTimeline
import com.jorgejbarra.console_twitter.domain.ShowUnknownCommand
import com.jorgejbarra.console_twitter.domain.ShowWall
import com.jorgejbarra.console_twitter.domain.follow.FollowSomeone
import com.jorgejbarra.console_twitter.domain.post.CreatePost

class Twitter(
    private val commandFactory: CommandFactory,
    private val createPost: CreatePost,
    private val showTimeline: ShowTimeline,
    private val showWall: ShowWall,
    private val followSomeone: FollowSomeone,
    private val unknownCommand: ShowUnknownCommand
) {
    fun execute(line: String) {

        when (val command = commandFactory.getCommand(line)) {
            is CreatePostCommand -> createPost.execute(command)
            is ShowTimeLineCommand -> showTimeline.execute(command)
            is ShowWallCommand -> showWall.execute(command)
            is FollowsCommand -> followSomeone.execute(command)
            is UnknownCommand -> unknownCommand.execute(command)
        }
    }
}
