package com.jorgejbarra.console_twitter.application.command

import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.post.PostText

sealed class Command {
    data class CreatePostCommand internal constructor(val user: UserName, val message: PostText) : Command()
    data class ShowTimeLineCommand internal constructor(val userName: UserName) : Command()
    data class ShowWallCommand internal constructor(val userName: UserName) : Command()
    data class FollowsCommand internal constructor(val source: UserName, val target: UserName) : Command()
    data class UnknownCommand internal constructor(val line: String) : Command()
}
