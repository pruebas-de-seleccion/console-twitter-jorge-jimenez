package com.jorgejbarra.console_twitter.application.command

import com.jorgejbarra.console_twitter.application.command.Command.*
import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.post.PostText

class CommandFactory {
    fun getCommand(line: String): Command {

        val args = line.split(" ")

        if (args.matchWithShowTimeLineCommand()) {
            return ShowTimeLineCommand(UserName(line))
        }

        if (args.matchWithFollowCommand()) {
            return FollowsCommand(UserName(args[0]), UserName(args[2]))
        }

        if (args.matchWithShowWallCommand()) {
            return ShowWallCommand(UserName(args[0]))
        }

        if (args.matchWithCreatePostCommand()) {
            return CreatePostCommand(UserName(args[0]), PostText(args.filterFirstPart().joinToString(" ")))
        }

        return UnknownCommand(line)
    }

    private fun List<String>.matchWithCreatePostCommand() = size > 2 && this[1] == "->"
    private fun List<String>.matchWithShowTimeLineCommand() = size == 1 && this[0].isNotEmpty()
    private fun List<String>.matchWithFollowCommand() = size == 3 && this[1] == "follows"
    private fun List<String>.matchWithShowWallCommand() = size == 2 && this[1] == "wall"

    private fun List<String>.filterFirstPart() = this.filterIndexed { index, _ -> (index > 1) }
}
