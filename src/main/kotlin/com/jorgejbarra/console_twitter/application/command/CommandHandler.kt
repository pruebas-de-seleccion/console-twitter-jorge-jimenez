package com.jorgejbarra.console_twitter.application.command

interface CommandHandler<T : Command> {
    fun execute(command: T)
}
