package com.jorgejbarra.console_twitter.domain

import java.time.Instant

interface Clock {
    fun now(): Instant
}
