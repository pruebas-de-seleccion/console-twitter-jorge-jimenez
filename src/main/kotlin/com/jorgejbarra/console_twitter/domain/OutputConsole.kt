package com.jorgejbarra.console_twitter.domain

interface OutputConsole {
    fun printLine(line: String)
}
