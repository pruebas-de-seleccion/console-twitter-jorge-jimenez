package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.ShowTimeLineCommand
import com.jorgejbarra.console_twitter.application.command.CommandHandler
import com.jorgejbarra.console_twitter.domain.post.PostPrinter
import com.jorgejbarra.console_twitter.domain.post.PostRepository

class ShowTimeline(private val postRepository: PostRepository, private val postPrinter: PostPrinter) :
    CommandHandler<ShowTimeLineCommand> {

    override fun execute(command: ShowTimeLineCommand) {
        postRepository.findPost(command.userName)
            .sortedWith(compareBy({ it.createAt }, { it.createAt }))
            .reversed()
            .forEach(postPrinter::print)
    }
}
