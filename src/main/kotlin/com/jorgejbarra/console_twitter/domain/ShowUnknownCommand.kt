package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.UnknownCommand
import com.jorgejbarra.console_twitter.application.command.CommandHandler

class ShowUnknownCommand(private val console: OutputConsole) : CommandHandler<UnknownCommand> {
    override fun execute(command: UnknownCommand) {
        console.printLine("Sorry I dont understand you when you say: '${command.line}'")
    }
}
