package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.ShowWallCommand
import com.jorgejbarra.console_twitter.application.command.CommandHandler
import com.jorgejbarra.console_twitter.domain.follow.FollowsRepository
import com.jorgejbarra.console_twitter.domain.post.Post
import com.jorgejbarra.console_twitter.domain.post.PostPrinter
import com.jorgejbarra.console_twitter.domain.post.PostRepository

class ShowWall(
    private val followsRepository: FollowsRepository,
    private val postRepository: PostRepository,
    private val postPrinter: PostPrinter
) : CommandHandler<ShowWallCommand> {

    override fun execute(command: ShowWallCommand) {
        postRepository.findPost(command.userName)
            .union(allFollowedUserPost(command))
            .sortedWith(compareBy({ it.createAt }, { it.createAt }))
            .reversed()
            .forEach(postPrinter::print)
    }

    private fun allFollowedUserPost(command: ShowWallCommand): List<Post> =
        followsRepository.getFollows(command.userName).map { postRepository.findPost(it) }.flatten()
}
