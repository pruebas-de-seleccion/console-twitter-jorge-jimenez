package com.jorgejbarra.console_twitter.domain

data class UserName(val value: String)
