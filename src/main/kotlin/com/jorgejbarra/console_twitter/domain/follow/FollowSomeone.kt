package com.jorgejbarra.console_twitter.domain.follow

import com.jorgejbarra.console_twitter.application.command.Command.FollowsCommand
import com.jorgejbarra.console_twitter.application.command.CommandHandler

class FollowSomeone(private var followsRepository: FollowsRepository) : CommandHandler<FollowsCommand> {
    override fun execute(command: FollowsCommand) {
        followsRepository.add(command.source, command.target)
    }
}
