package com.jorgejbarra.console_twitter.domain.follow

import com.jorgejbarra.console_twitter.domain.UserName

interface FollowsRepository {
    fun add(source: UserName, target: UserName)
    fun getFollows(userName: UserName): Collection<UserName>
}
