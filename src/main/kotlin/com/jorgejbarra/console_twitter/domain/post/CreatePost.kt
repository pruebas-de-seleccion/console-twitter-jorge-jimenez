package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.application.command.Command.CreatePostCommand
import com.jorgejbarra.console_twitter.application.command.CommandHandler
import com.jorgejbarra.console_twitter.domain.Clock

class CreatePost(private val postRepository: PostRepository, private val clock: Clock) :
    CommandHandler<CreatePostCommand> {

    override fun execute(command: CreatePostCommand) {
        postRepository.store(Post(command.user, command.message, clock.now()))
    }
}

