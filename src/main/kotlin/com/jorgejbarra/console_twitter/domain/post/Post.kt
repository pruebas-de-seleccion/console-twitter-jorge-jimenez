package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.domain.UserName
import java.time.Instant

data class Post(val userName: UserName, val message: PostText, val createAt: Instant)
