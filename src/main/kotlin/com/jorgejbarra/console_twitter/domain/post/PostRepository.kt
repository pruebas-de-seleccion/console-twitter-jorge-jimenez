package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.domain.UserName

interface PostRepository {
    fun store(post: Post)
    fun findPost(userName: UserName): List<Post>
}
