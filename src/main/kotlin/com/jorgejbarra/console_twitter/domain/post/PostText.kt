package com.jorgejbarra.console_twitter.domain.post

data class PostText(val value: String)
