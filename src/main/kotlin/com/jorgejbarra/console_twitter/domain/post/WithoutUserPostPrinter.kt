package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.domain.Clock
import com.jorgejbarra.console_twitter.domain.OutputConsole
import java.time.Duration

abstract class PostPrinter(private val clock: Clock, private val console: OutputConsole) {
    fun print(post: Post) {
        val timeAgo = Duration.between(post.createAt, clock.now()).formatTimeAgo()
        console.printLine("${post.serializePost()} ($timeAgo)")
    }

    abstract internal fun Post.serializePost(): String

    internal fun Duration.formatTimeAgo(): String {
        val hours: Long = toHours()
        if (hours >= 1) {
            return "$hours hours ago"
        }

        val minutes: Long = minusHours(hours).toMinutes()
        if (minutes >= 1) {
            return "$minutes minutes ago"
        }

        val seconds: Long = minusHours(hours).minusMinutes(minutes).toSeconds()
        return "$seconds seconds ago"
    }
}

class WithoutUserPostPrinter(clock: Clock, console: OutputConsole) : PostPrinter(clock, console) {
    override fun Post.serializePost(): String = message.value
}

class WithUserPostPrinter(clock: Clock, console: OutputConsole) : PostPrinter(clock, console) {
    override fun Post.serializePost(): String = "${userName.value} - ${message.value}"
}
