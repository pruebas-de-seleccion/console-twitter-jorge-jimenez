package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.follow.FollowsRepository
import java.util.concurrent.ConcurrentHashMap

class InMemoryFollowsRepository : FollowsRepository {
    private val storage = ConcurrentHashMap<UserName, Set<UserName>>()

    override fun add(source: UserName, target: UserName) {
        storage.getOrDefault(source, emptySet()).toMutableSet()
            .apply { add(target) }
            .toSet()
            .apply { storage[source] = this }
    }

    override fun getFollows(userName: UserName): Collection<UserName> {
        return storage.getOrDefault(userName, emptySet())
    }
}
