package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.post.Post
import com.jorgejbarra.console_twitter.domain.post.PostRepository

class InMemoryPostRepository : PostRepository {
    private val storage = mutableListOf<Post>()

    override fun store(post: Post) {
        storage.add(post)
    }

    override fun findPost(userName: UserName): List<Post> = storage.filter { it.userName == userName }
}
