package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.domain.Clock
import java.time.Instant

class SystemClock : Clock {
    override fun now(): Instant = Instant.now()
}
