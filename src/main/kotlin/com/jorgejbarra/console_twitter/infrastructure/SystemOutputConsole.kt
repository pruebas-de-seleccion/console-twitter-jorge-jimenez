package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.domain.OutputConsole
import kotlin.io.println as systemPrintLine

class SystemOutputConsole : OutputConsole {
    override fun printLine(line: String) {
        systemPrintLine(line)
    }
}
