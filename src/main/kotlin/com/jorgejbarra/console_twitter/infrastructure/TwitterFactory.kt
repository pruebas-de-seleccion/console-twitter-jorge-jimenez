package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.application.Twitter
import com.jorgejbarra.console_twitter.application.command.CommandFactory
import com.jorgejbarra.console_twitter.domain.ShowTimeline
import com.jorgejbarra.console_twitter.domain.ShowUnknownCommand
import com.jorgejbarra.console_twitter.domain.ShowWall
import com.jorgejbarra.console_twitter.domain.follow.FollowSomeone
import com.jorgejbarra.console_twitter.domain.post.CreatePost
import com.jorgejbarra.console_twitter.domain.post.WithUserPostPrinter
import com.jorgejbarra.console_twitter.domain.post.WithoutUserPostPrinter

object TwitterFactory {
    fun getTwitterInstance(): Twitter {
        val postRepository = InMemoryPostRepository()
        val followsRepository = InMemoryFollowsRepository()
        return Twitter(
            commandFactory = CommandFactory(),
            createPost = CreatePost(postRepository, SystemClock()),
            showTimeline = ShowTimeline(
                postRepository = postRepository,
                postPrinter = WithoutUserPostPrinter(SystemClock(), SystemOutputConsole())
            ),
            showWall = ShowWall(
                followsRepository = followsRepository,
                postRepository = postRepository,
                postPrinter = WithUserPostPrinter(SystemClock(), SystemOutputConsole())
            ),
            followSomeone = FollowSomeone(followsRepository),
            unknownCommand = ShowUnknownCommand(SystemOutputConsole())
        )
    }
}
