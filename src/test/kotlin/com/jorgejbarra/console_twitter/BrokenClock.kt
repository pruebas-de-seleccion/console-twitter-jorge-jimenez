package com.jorgejbarra.console_twitter

import com.jorgejbarra.console_twitter.domain.Clock
import java.time.Instant

class BrokenClock(private var stoppedInstant: Instant) : Clock {
    override fun now() = stoppedInstant

    fun changeNow(otherInstant: Instant) {
        this.stoppedInstant = otherInstant
    }
}
