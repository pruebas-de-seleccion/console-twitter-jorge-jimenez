package com.jorgejbarra.console_twitter

import java.io.ByteArrayOutputStream
import java.io.PrintStream

class ConsoleWrapperForTest {
    private val outputStream = ByteArrayOutputStream()

    init {
        System.setOut(PrintStream(outputStream))
    }

    fun reset() {
        outputStream.reset()
    }

    fun getLines() = outputStream.toString().split("\n").filter(String::isNotBlank)
}
