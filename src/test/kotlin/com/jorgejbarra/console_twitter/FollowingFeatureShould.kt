package com.jorgejbarra.console_twitter

import com.jorgejbarra.console_twitter.TwitterFactory.loadInitialScenario
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class FollowingFeatureShould {
    private val output = ConsoleWrapperForTest()

    @BeforeEach
    internal fun setUp() {
        output.reset()
    }

    @Test
    internal fun `show only your post when dont have followed`() {
        //given
        val twitter = TwitterFactory.twitterForTest().loadInitialScenario()

        //when
        twitter.execute("Charlie wall")

        //then
        assertThat(output.getLines()).containsExactly(
            "Charlie - I'm in New York today! Anyone want to have a coffee? (15 seconds ago)"
        )
    }

    @Test
    internal fun `show all post of followed user in wall`() {
        //given
        val twitter = TwitterFactory.twitterForTest().loadInitialScenario()

        //when
        twitter.execute("Charlie follows Alice")
        twitter.execute("Charlie follows Bob")
        twitter.execute("Charlie wall")

        //then
        assertThat(output.getLines()).containsExactly(
            "Charlie - I'm in New York today! Anyone want to have a coffee? (15 seconds ago)",
            "Bob - Good game though. (1 minutes ago)",
            "Bob - Damn! We lost! (2 minutes ago)",
            "Alice - I love the weather today (5 minutes ago)"
        )
    }
}
