package com.jorgejbarra.console_twitter

import com.jorgejbarra.console_twitter.TwitterFactory.loadInitialScenario
import com.jorgejbarra.console_twitter.TwitterFactory.twitterForTest
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class ReadingFeatureShould {
    private val output = ConsoleWrapperForTest()

    @BeforeEach
    internal fun setUp() {
        output.reset()
    }

    @Test
    internal fun `reading alice`() {
        //given
        val twitter = twitterForTest().loadInitialScenario()

        //when
        twitter.execute("Alice")

        //then
        assertThat(output.getLines()).containsExactly("I love the weather today (5 minutes ago)")
    }

    @Test
    internal fun `reading Bob`() {
        //given
        val twitter = twitterForTest().loadInitialScenario()

        //when
        twitter.execute("Bob")

        //then
        assertThat(output.getLines()).containsExactly(
            "Good game though. (1 minutes ago)",
            "Damn! We lost! (2 minutes ago)"
        )
    }
}
