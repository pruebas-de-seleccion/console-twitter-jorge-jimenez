package com.jorgejbarra.console_twitter

import com.jorgejbarra.console_twitter.application.Twitter
import com.jorgejbarra.console_twitter.application.command.CommandFactory
import com.jorgejbarra.console_twitter.domain.ShowTimeline
import com.jorgejbarra.console_twitter.domain.ShowUnknownCommand
import com.jorgejbarra.console_twitter.domain.ShowWall
import com.jorgejbarra.console_twitter.domain.follow.FollowSomeone
import com.jorgejbarra.console_twitter.domain.post.CreatePost
import com.jorgejbarra.console_twitter.domain.post.WithUserPostPrinter
import com.jorgejbarra.console_twitter.domain.post.WithoutUserPostPrinter
import com.jorgejbarra.console_twitter.infrastructure.InMemoryFollowsRepository
import com.jorgejbarra.console_twitter.infrastructure.InMemoryPostRepository
import com.jorgejbarra.console_twitter.infrastructure.SystemOutputConsole
import java.time.Duration
import java.time.Instant

internal object TwitterFactory {
    private val clock = BrokenClock(Instant.now())

    fun twitterForTest(): Twitter {
        val postRepository = InMemoryPostRepository()
        val followsRepository = InMemoryFollowsRepository()
        return Twitter(
            commandFactory = CommandFactory(),
            createPost = CreatePost(postRepository, clock),
            showTimeline = ShowTimeline(postRepository, WithoutUserPostPrinter(clock, SystemOutputConsole())),
            showWall = ShowWall(followsRepository, postRepository, WithUserPostPrinter(clock, SystemOutputConsole())),
            followSomeone = FollowSomeone(followsRepository),
            unknownCommand = ShowUnknownCommand(SystemOutputConsole())
        )
    }

    fun Twitter.loadInitialScenario(): Twitter {
        val zeroTime = Instant.now()
        clock.changeNow(zeroTime.minus(Duration.ofMinutes(5)))
        execute("Alice -> I love the weather today")

        clock.changeNow(zeroTime.minus(Duration.ofMinutes(1)))
        execute("Bob -> Good game though.")

        clock.changeNow(zeroTime.minus(Duration.ofMinutes(2)))
        execute("Bob -> Damn! We lost!")

        clock.changeNow(zeroTime.minus(Duration.ofSeconds(15)))
        execute("Charlie -> I'm in New York today! Anyone want to have a coffee?")

        clock.changeNow(zeroTime)

        return this
    }
}
