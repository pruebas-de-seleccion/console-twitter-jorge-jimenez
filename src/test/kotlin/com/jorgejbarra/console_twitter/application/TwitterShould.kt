package com.jorgejbarra.console_twitter.application

import com.jorgejbarra.console_twitter.application.command.Command.*
import com.jorgejbarra.console_twitter.application.command.CommandFactory
import com.jorgejbarra.console_twitter.domain.ShowTimeline
import com.jorgejbarra.console_twitter.domain.ShowUnknownCommand
import com.jorgejbarra.console_twitter.domain.ShowWall
import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.follow.FollowSomeone
import com.jorgejbarra.console_twitter.domain.post.CreatePost
import com.jorgejbarra.console_twitter.domain.post.PostText
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.Test

internal class TwitterShould {
    private val followSomeone = mock<FollowSomeone>()
    private val showWall = mock<ShowWall>()
    private val unknownCommand = mock<ShowUnknownCommand>()
    private val createPost = mock<CreatePost>()
    private val showTimeline = mock<ShowTimeline>()

    private val twitter = Twitter(CommandFactory(), createPost, showTimeline, showWall, followSomeone, unknownCommand)

    @Test
    internal fun `do "unknown-command-response" when command is unknown`() {
        twitter.execute("unknown command")

        verify(unknownCommand).execute(UnknownCommand("unknown command"))
    }

    @Test
    internal fun `store when command is post`() {
        twitter.execute("Alice -> I love the weather today")

        verify(createPost)
            .execute(CreatePostCommand(UserName("Alice"), PostText("I love the weather today")))
    }

    @Test
    internal fun `show timeline when command is reading`() {
        twitter.execute("Alice")

        verify(showTimeline).execute(ShowTimeLineCommand(UserName("Alice")))
    }

    @Test
    internal fun `show wall when command is wall`() {
        twitter.execute("Alice wall")

        verify(showWall).execute(ShowWallCommand(UserName("Alice")))
    }

    @Test
    internal fun `execute follow when command is follow`() {
        twitter.execute("Alice follows Bob")

        verify(followSomeone).execute(FollowsCommand(UserName("Alice"), UserName("Bob")))
    }
}
