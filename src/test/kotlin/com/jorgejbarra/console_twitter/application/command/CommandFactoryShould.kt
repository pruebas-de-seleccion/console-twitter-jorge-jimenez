package com.jorgejbarra.console_twitter.application.command

import com.jorgejbarra.console_twitter.application.command.Command.*
import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.post.PostText
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class CommandFactoryShould {
    private val commandFactory = CommandFactory()

    @Test
    internal fun `create unknown command when line is empty`() {
        val command = commandFactory.getCommand("")

        assertThat(command).isEqualTo(UnknownCommand(""))
    }

    @Test
    internal fun `create show timeline command when line contains only one word`() {
        val command = commandFactory.getCommand("Bob")

        assertThat(command).isEqualTo(ShowTimeLineCommand(UserName("Bob")))
    }

    @Test
    internal fun `create show wall command when line contains two word and wall command`() {
        val command = commandFactory.getCommand("Bob wall")

        assertThat(command).isEqualTo(ShowWallCommand(UserName("Bob")))
    }

    @Test
    internal fun `create follows command when line contains three word and follows command`() {
        val command = commandFactory.getCommand("Bob follows Charlie")

        assertThat(command).isEqualTo(FollowsCommand(UserName("Bob"), UserName("Charlie")))
    }

    @Test
    internal fun `create create post command when line contains more than two word and post command`() {
        val command = commandFactory.getCommand("Charlie -> Good game though.")

        assertThat(command).isEqualTo(
            CreatePostCommand(
                UserName("Charlie"),
                PostText("Good game though.")
            )
        )
    }
}
