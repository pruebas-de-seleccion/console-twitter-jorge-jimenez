package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.ShowTimeLineCommand
import com.jorgejbarra.console_twitter.domain.post.PostFactory.aPost
import com.jorgejbarra.console_twitter.domain.post.PostPrinter
import com.jorgejbarra.console_twitter.domain.post.PostRepository
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import org.junit.jupiter.api.Test
import java.time.Instant.now

internal class ShowTimelineShould {

    private val postRepository = mock<PostRepository>()
    private val postPrinter = mock<PostPrinter>()
    private val showTimeline = ShowTimeline(postRepository, postPrinter)

    @Test
    internal fun `print post text of a given user`() {
        val bobUser = UserName("Bob")
        val aPost = aPost().copy(userName = bobUser)
        val aOtherPost = aPost().copy(userName = bobUser)
        whenever(postRepository.findPost(bobUser)).thenReturn(listOf(aPost, aOtherPost))

        showTimeline.execute(ShowTimeLineCommand(bobUser))

        verify(postPrinter).print(aPost)
        verify(postPrinter).print(aOtherPost)
    }

    @Test
    internal fun `order post by date`() {
        val oldPost = aPost().copy(createAt = now().minusSeconds(44))
        val recentPost = aPost().copy(createAt = now().minusSeconds(23))
        whenever(postRepository.findPost(recentPost.userName)).thenReturn(listOf(oldPost, recentPost))

        showTimeline.execute(ShowTimeLineCommand(oldPost.userName))

        inOrder(postPrinter).apply {
            verify(postPrinter).print(recentPost)
            verify(postPrinter).print(oldPost)
        }
    }
}
