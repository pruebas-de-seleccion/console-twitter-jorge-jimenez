package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.ShowWallCommand
import com.jorgejbarra.console_twitter.domain.follow.FollowsRepository
import com.jorgejbarra.console_twitter.domain.post.PostFactory.aPost
import com.jorgejbarra.console_twitter.domain.post.PostPrinter
import com.jorgejbarra.console_twitter.domain.post.PostRepository
import com.jorgejbarra.console_twitter.infrastructure.InMemoryFollowsRepository
import com.jorgejbarra.console_twitter.infrastructure.InMemoryPostRepository
import com.nhaarman.mockito_kotlin.inOrder
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.verifyNoMoreInteractions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Instant.now

internal class ShowWallTest {
    private var postPrinterMock = mock<PostPrinter>()
    private lateinit var followsRepository: FollowsRepository
    private lateinit var postRepository: PostRepository
    private lateinit var showWall: ShowWall

    @BeforeEach
    internal fun setUp() {
        followsRepository = InMemoryFollowsRepository()
        postRepository = InMemoryPostRepository()
        showWall = ShowWall(followsRepository, postRepository, postPrinterMock)
    }

    @Test
    internal fun `show only there post when they don't follow to anyone`() {

        val bob = UserName("Bob")
        val charlie = UserName("Charlie")
        val bobPost = aPost().copy(userName = bob)
        val charliePost = aPost().copy(userName = charlie)
        postRepository.apply {
            store(bobPost)
            store(charliePost)
        }

        showWall.execute(ShowWallCommand(bob))

        verify(postPrinterMock).print(bobPost)
        verifyNoMoreInteractions(postPrinterMock)
    }

    @Test
    internal fun `show  all post of followed user ordered by date`() {

        val bob = UserName("Bob")
        val charlie = UserName("Charlie")
        val bobPost = aPost().copy(userName = bob, createAt = now().minusSeconds(2))
        val charlieRecentPost = aPost().copy(userName = charlie)
        val charlieOldPost = aPost().copy(userName = charlie, createAt = now().minusSeconds(200))
        postRepository.apply {
            store(bobPost)
            store(charlieRecentPost)
            store(charlieOldPost)
        }
        followsRepository.add(bob, charlie)

        showWall.execute(ShowWallCommand(bob))

        inOrder(postPrinterMock).apply {
            verify(postPrinterMock).print(charlieRecentPost)
            verify(postPrinterMock).print(bobPost)
            verify(postPrinterMock).print(charlieOldPost)

            verifyNoMoreInteractions(postPrinterMock)
        }
    }
}
