package com.jorgejbarra.console_twitter.domain

import com.jorgejbarra.console_twitter.application.command.Command.UnknownCommand
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.Test

internal class UnknownCommandShould {

    private val console = mock<OutputConsole>()

    @Test
    internal fun `do "unknown-command-response"`() {
        val twitterConsole = ShowUnknownCommand(console)

        twitterConsole.execute(UnknownCommand("something strange"))

        verify(console).printLine("Sorry I dont understand you when you say: 'something strange'")
    }
}
