package com.jorgejbarra.console_twitter.domain.follow

import com.jorgejbarra.console_twitter.application.command.Command.FollowsCommand
import com.jorgejbarra.console_twitter.domain.UserName
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.Test

internal class FollowSomeoneShould {
    private val followsRepository = mock<FollowsRepository>()
    private val followSomeone = FollowSomeone(followsRepository)

    @Test
    internal fun `store follow relationship`() {
        val source = UserName("Jorge")
        val target = UserName("Trisha")

        followSomeone.execute(FollowsCommand(source, target))

        verify(followsRepository).add(source, target)
    }
}
