package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.BrokenClock
import com.jorgejbarra.console_twitter.application.command.Command.CreatePostCommand
import com.jorgejbarra.console_twitter.domain.UserName
import com.jorgejbarra.console_twitter.domain.post.PostFactory.aPost
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.Test
import java.time.Instant

internal class CreatePostShould {
    private val clock = BrokenClock(Instant.now())
    private val postRepository = mock<PostRepository>()
    private val createPost =
        CreatePost(postRepository, clock)

    @Test
    internal fun `store new post with current time`() {
        val expectedUserName = "Charlie"
        val expectedPostText = "I'm in New York today! Anyone wants to have a coffee?"

        createPost.execute(
            CreatePostCommand(
                UserName(expectedUserName),
                PostText(expectedPostText)
            )
        )

        verify(postRepository).store(aPost(expectedUserName, expectedPostText, clock.now()))
    }
}
