package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.domain.UserName
import java.time.Instant

object PostFactory {
    fun aPost(
        userName: String = "Jorge",
        message: String = "This is a simple message for testing purposes",
        time: Instant = Instant.now()
    ) = Post(UserName(userName), PostText(message), time)
}
