package com.jorgejbarra.console_twitter.domain.post

import com.jorgejbarra.console_twitter.BrokenClock
import com.jorgejbarra.console_twitter.domain.OutputConsole
import com.jorgejbarra.console_twitter.domain.post.PostFactory.aPost
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import org.junit.jupiter.api.Test
import java.time.Duration.ofMinutes
import java.time.Duration.ofSeconds
import java.time.Instant

internal class PostPrinterShould {
    private val console = mock<OutputConsole>()
    private val clock = BrokenClock(Instant.now())
    private val postPrinter = WithoutUserPostPrinter(clock, console)

    @Test
    internal fun `show time as hour gap when print a post more than 1 hour ago`() {
        val moreThanOneHourAgo = clock.now().minus(ofMinutes(61))

        postPrinter.print(aPost(message = "This clock is ok xD", time = moreThanOneHourAgo))

        verify(console).printLine("This clock is ok xD (1 hours ago)")
    }

    @Test
    internal fun `show time as minute gap when print a post less than 1 hour ago`() {
        val lessThanOneHourAgo = clock.now().minus(ofMinutes(59))

        postPrinter.print(aPost(message = "This clock is ok :P", time = lessThanOneHourAgo))

        verify(console).printLine("This clock is ok :P (59 minutes ago)")
    }

    @Test
    internal fun `show time as minute gap when print a post more than 1 minute ago`() {
        val moreThanOneMinuteAgo = clock.now().minus(ofSeconds(61))

        postPrinter.print(aPost(message = "I pass of the plurals :*", time = moreThanOneMinuteAgo))

        verify(console).printLine("I pass of the plurals :* (1 minutes ago)")
    }

    @Test
    internal fun `show time as minute gap when print a post more lest 1 minute ago`() {
        val lessThanOneMinuteAgo = clock.now().minus(ofSeconds(59))

        postPrinter.print(aPost(message = "That is all, more is unnecessary :)", time = lessThanOneMinuteAgo))

        verify(console).printLine("That is all, more is unnecessary :) (59 seconds ago)")
    }
}
