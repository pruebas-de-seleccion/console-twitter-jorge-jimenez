package com.jorgejbarra.console_twitter.infrastructure

import com.jorgejbarra.console_twitter.domain.UserName
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class FollowsRepositoryShould {

    @Test
    internal fun `return empty follows list when user is unknown`() {
        val followRepository = InMemoryFollowsRepository()

        val follows = followRepository.getFollows(UserName("Unknown"))

        assertThat(follows).isEmpty()
    }

    @Test
    internal fun `do not duplicate follows`() {
        val followRepository = InMemoryFollowsRepository()
        followRepository.add(UserName("Jorge"), UserName("Trisha"))
        followRepository.add(UserName("Jorge"), UserName("Trisha"))

        val follows = followRepository.getFollows(UserName("Jorge"))

        assertThat(follows).containsExactly(UserName("Trisha"))
    }

    @Test
    internal fun `return follows of given user`() {
        val followRepository = InMemoryFollowsRepository()
        followRepository.add(UserName("Jorge"), UserName("Trisha"))
        followRepository.add(UserName("Jorge"), UserName("Joan"))
        followRepository.add(UserName("Vanesa"), UserName("Trisha"))

        val follows = followRepository.getFollows(UserName("Jorge"))

        assertThat(follows).containsExactlyInAnyOrder(UserName("Trisha"), UserName("Joan"))
    }
}
